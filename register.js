const firstNameInput = document.querySelector('#first-name-input');
const lastNameInput = document.querySelector('#last-name-input');
const mobileNoNameInput = document.querySelector('#mobile-num-input');
const emailInput = document.querySelector('#email-input');
const passwordInput = document.querySelector('#password-input');

document.querySelector('#form-register').addEventListener('submit', (e) => {
  e.preventDefault();
  fetch('http://localhost:4000/users', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      firstName: firstNameInput.value,
      lastName: lastNameInput.value,
      email: emailInput.value,
      mobileNo: mobileNoNameInput.value,
      password: passwordInput.value,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      console.log(data);
    });
});
