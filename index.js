const coursesDiv = document.querySelector('#courses-div');
// console.log(coursesDiv);
// console.log(coursesDiv.innerHTML);

const mainDiv = document.querySelector('#main-div');
// console.log(mainDiv.innerHTML);

// mainDiv.innerHTML = "<h1>Batch 123 is awesome</h1>";
// mainDiv.innerHTML += "<p>Full Stack Developers!</p>";
// mainDiv.innerHTML = mainDiv.innerHTML + "<p>I am </p>";

fetch('http://localhost:4000/courses/getActiveCourses')
  .then((res) => res.json())
  .then((data) => {
    let coursesCards = '';
    data.forEach((course) => {
      coursesCards += `
            <div class="card">
                <h4>${course.name}</h4>
                <p>${course.description}</p>
                <span>Price: PHP ${course.price}</span>
            </div>
        `;
    });
    coursesDiv.innerHTML = coursesCards;

    // const coursesCards = document.createElement("div");
    // data.forEach((course) => {
    //   const div = document.createElement("div");
    //   div.classList.add("card");
    //   const heading = document.createElement("h4");
    //   heading.textContent = course.name;
    //   div.append(heading);

    //   const descriptionParagraph = document.createElement("p");
    //   descriptionParagraph.textContent = course.description;

    //   const priceSpan = document.createElement("span");
    //   priceSpan.textContent = `Price: PHP ${course.price}`;

    //   coursesCards.append(div, descriptionParagraph, priceSpan);
    // });
    // coursesDiv.append(coursesCards);
  });
