const emailInput = document.querySelector('#email-input');
const passwordInput = document.querySelector('#password-input');

document.querySelector('#form-login').addEventListener('submit', (e) => {
  e.preventDefault();
  fetch('http://localhost:4000/users/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      email: emailInput.value,
      password: passwordInput.value,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      console.log(data);
      if (data.token) {
        localStorage.setItem('token', data.token);
      }
    });
});
