const token = localStorage.getItem('token');

const profileName = document.querySelector('#profile-name');
const profileEmail = document.querySelector('#profile-email');
const profileMobile = document.querySelector('#profile-mobile');

fetch('http://localhost:4000/users/getUserDetails', {
  headers: {
    authorization: `Bearer ${token}`,
  },
})
  .then((res) => res.json())
  .then((data) => {
    console.log(data);
    profileName.textContent = `Hello, ${data.firstName} ${data.lastName}`;
    profileEmail.textContent = `Email: ${data.email}`;
    profileMobile.textContent = `Mobile: ${data.mobileNo}`;
  });
