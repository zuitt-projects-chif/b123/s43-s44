const token = localStorage.getItem('token');
const courseNameInput = document.querySelector('#course-name-input');
const courseDescriptionInput = document.querySelector('#description-input');
const coursePriceInput = document.querySelector('#price-input');

document.querySelector('#form-course').addEventListener('submit', (e) => {
  e.preventDefault();
  console.log(courseNameInput.value);
  console.log(courseDescriptionInput.value);
  console.log(coursePriceInput.value);

  fetch('http://localhost:4000/courses', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      authorization: `Bearer ${token}`,
    },
    body: JSON.stringify({
      name: courseNameInput.value,
      description: courseDescriptionInput.value,
      price: coursePriceInput.value,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      console.log(data);
      if (data.token) {
        localStorage.setItem('token', data.token);
      }
    });
});
